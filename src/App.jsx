import { Pokedex } from './components/Pokedex';
import './styles/App.css'

function App() {
  return (
    <div className="App">
      <Pokedex />
    </div>
  )
}

export default App
