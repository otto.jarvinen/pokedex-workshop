import React from "react";
import { LeftControlArea } from "./LeftControlArea";
import { Header } from "./Header";
import { ImageArea } from "./ImageArea";
import { InfoArea } from "./InfoArea";
import { RightControlArea } from "./RightControlArea";
import { PreviousSearches } from "./PreviousSearches";

import "../styles/pokedex.css";

export const Pokedex = () => {
  return (
    <div className="container">
      <div className="left">
        <Header />
        <ImageArea />
        <LeftControlArea />
      </div>
      <div className="right">
        <InfoArea />
        <PreviousSearches />
        <RightControlArea />
      </div>
    </div>
  );
};
