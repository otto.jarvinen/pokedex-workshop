import React from "react";
import "../styles/right-control-area.css";

export const RightControlArea = () => {
  return (
    <div className="right-control-area">
      <div className="small-black-buttons-container">
        <button className="small-black-button small-black-button-left" />
        <button className="small-black-button small-black-button-right" />
      </div>
      <div className="middle-controls-container">
        <div className="gray-buttons-container">
          <button className="gray-button" />
          <button className="gray-button" />
        </div>
        <div className="yellow-indicator-wrapper">
          <div className="yellow-indicator"></div>
        </div>
      </div>
      <div className="big-black-buttons-container">
        <button className="big-black-button" />
        <button className="big-black-button" />
      </div>
    </div>
  );
};
