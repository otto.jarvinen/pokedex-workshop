import React from "react";
import "../styles/header.css";

export const Header = () => {
  return (
    <div className="header">
      <div className="indicator-wrapper">
        <div className="indicator" />
      </div>
      <div className="header-indicators-container">
        <div className="small-header-indicator small-header-indicator-red"></div>
        <div className="small-header-indicator small-header-indicator-yellow"></div>
        <div className="small-header-indicator small-header-indicator-green"></div>
      </div>
    </div>
  );
};
