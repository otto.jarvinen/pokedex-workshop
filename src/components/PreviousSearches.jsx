import React from "react";
import "../styles/previous-searches.css";

export const PreviousSearches = () => {
  return (
    <div className="previous-searches">
      {[...Array(10)].map((e, index) => (
        <div className="previous-search-button" key={index} />
      ))}
    </div>
  );
};
