import React from "react";
import "../styles/image-area.css";

export const ImageArea = () => {
  return (
    <div className="image-area">
      <div className="image-frame">
        <div className="frame-top-indicator-container">
          <div className="frame-top-indicator"></div>
          <div className="frame-top-indicator"></div>
        </div>
        <div className="image-screen">
        </div>
        <div className="frame-buttons">
          <div className="frame-circle-button-wrapper">
            <button className="frame-circle-button"></button>
          </div>
          <div className="frame-black-lines-container">
            <div className="frame-black-lines"></div>
            <div className="frame-black-lines"></div>
          </div>
        </div>
      </div>
    </div>
  );
};
