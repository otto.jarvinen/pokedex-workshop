import React from "react";
import "../styles/left-control-area.css";

export const LeftControlArea = () => {
  return (
    <div className="left-control-area">
      <div className="search-button-wrapper">
        <button
          className="search-button"
        ></button>
      </div>
      <div className="selection-controller-container">
        <div className="selection-buttons-container">
          <button className="selection-button selection-button-red" />
          <button className="selection-button selection-button-blue" />
        </div>
        <div className="search-field-wrapper">
          <input
            className="search-field"
          />
        </div>
      </div>
      <div className="arrow-keys-wrapper">
        <div className="arrow-keys">
          <button className="arrow arrow-up" />
          <button
            className="arrow arrow-right"
          />
          <button className="arrow arrow-down" />
          <button
            className="arrow arrow-left"
          />
          <button className="arrow arrow-center" />
        </div>
      </div>
    </div>
  );
};
